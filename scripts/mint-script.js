const hre = require("hardhat");
async function main() {
    const NFT = await hre.ethers.getContractFactory("MyNFT");
    const URI = "ipfs://QmQzLgLeb8X5nFheiXzDNxjEQ5sjQG4k5GaeRR2zMDpMBi"
    const WALLET_ADDRESS = "0xAfe703e4f7F0C90f1837b8cAeE691d2B8AE24654"
    const CONTRACT_ADDRESS = "0xcDe0360C462CaA9863D429bF9a11414296F29A11"
    const contract = NFT.attach(CONTRACT_ADDRESS);
    await contract.mint(WALLET_ADDRESS, URI);
    console.log("NFT minted:", contract);
}
main().then(() => process.exit(0)).catch(error => {
    console.error(error);
    process.exit(1);
});