const { expect } = require("chai");
describe("NFT", function () {
  it("It should deploy the contract, mint a token, and resolve to the right URI", async function () {
    const NFT = await ethers.getContractFactory("MyNFT");
    const nft = await NFT.deploy();
    const URI = "ipfs://QmWJBNeQAm9Rh4YaW8GFRnSgwa4dN889VKm9poc2DQPBkv";
    await nft.deployed();
    await nft.mint("0xCA736a2b2c1B4B8C8b5E2AE73B269c49f3EFaCde", URI)
    expect(await nft.tokenURI(1)).to.equal(URI)
  });
});